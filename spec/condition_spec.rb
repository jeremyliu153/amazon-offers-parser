require_relative '../lib/condition.rb'

RSpec.describe Condition do 
	# new_html = ''
	# used_verygood_html = ''
	# used_acceptable_html = ''

	# html_arr = [new_html, used_verygood_html, used_acceptable_html]
	# context_arr = ["New", "Used - Very good", "Used - Acceptable"]
	# new_vals = [true, false, false]
	# used_vals = [false, true, true]
	# condition = ['NEW', 'USED', 'USED']
	# sub_condition = ['NEW', 'VERYGOOD', 'ACCEPTABLE']

	# context_arr.each_with_index do |v, i|
	# 	context v do
	# 		before(:example) do
	# 			html = html_arr[i]
	# 			@cond = Condition.new(html)
	# 			@cond.parse
	# 		end
	# 		it "#new? should return #{new_vals[i]}" do
	# 			expect(@cond.new?).to eq(new_vals[i])
	# 		end

	# 		it "#used? should return #{used_vals[i]}" do
	# 			expect(@cond.used?).to eq(used_vals[i])
	# 		end

	# 		it "condition should be #{condition[i]}" do
	# 	        expect(@cond.condition.upcase).to eq(condition[i])
	# 		end	

	# 		it "sub_condition should be #{sub_condition[i]}" do
	# 		    expect(@cond.sub_condition.upcase).to eq(sub_condition[i])
	# 	    end
	# 	end
 #    end

    context "New" do
        before(:example) do
          html = '<div class="a-column a-span3 olpConditionColumn" role="gridcell"><div id="offerCondition" class="a-section a-spacing-small"><span id="olpNew" class="a-size-medium olpCondition a-text-bold"> New </span></div></div>'
	      @cond = Condition.new(html)
	      @cond.parse
        end

        it '#new? should return true' do
          expect(@cond.new?).to eq(true)
        end

        it '#used? should return false' do
          expect(@cond.used?).to eq(false)
        end
		
	    it "condition should be new" do
	      expect(@cond.condition.upcase).to eq('NEW')
		end	

		it "sub_condition should be new" do
	      expect(@cond.sub_condition.upcase).to eq('NEW')
	    end
    end 

    context "Used - Very Good" do
        before(:example) do
          html = '<div class="a-column a-span3 olpConditionColumn" role="gridcell"><div id="offerCondition" class="a-section a-spacing-small"><span id="olpUsed" class="a-size-medium olpCondition a-text-bold"> Used - <span id="offerSubCondition">Very Good</span></span></div><div class="comments" style="display: inline-block;"> Item will come in original packaging. Packaging will be damaged.</div><noscript><div class="comments">Item will come in original packaging. Packaging will be damaged.</div></noscript> </div>'
	      @cond = Condition.new(html)
	      @cond.parse
        end
        
        it '#new? should return false' do
          expect(@cond.new?).to eq(false)
        end

        it '#used? should return true' do
          expect(@cond.used?).to eq(true)
        end

	    it "condition should be used" do
	      expect(@cond.condition.upcase).to eq('USED')
		end	

		it "sub_condition should be Very good" do
	      expect(@cond.sub_condition.upcase).to eq('VERYGOOD')
	    end
    end
	
end