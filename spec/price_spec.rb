require_relative '../lib/price.rb'

RSpec.describe Price do 
	price_html_block='<div class="a-column a-span2 olpPriceColumn" role="gridcell"> <span class="a-size-large a-color-price olpOfferPrice a-text-bold"> $12.99 </span><p class="olpShippingInfo"><span class="a-color-secondary"> &amp; <b>FREE Shipping</b></span></p></div>'
  price_html_block1='<div class="a-column a-span2 olpPriceColumn" role="gridcell"><span class="a-size-large a-color-price olpOfferPrice a-text-bold">CDN$ 19.99</span><p class="olpShippingInfo"><span class="a-color-secondary"> + <span class="olpShippingPrice">CDN$ 5.54</span><span class="olpShippingPriceText">shipping</span></span></p>'

  describe '#initialize' do
     price = Price.new(price_html_block)
     it "should parse sale price" do
        expect(price.sale_price).to eq(12.99)
      end
      
      it "should parse shipping fee" do
        expect(price.parse_shipping_fee).to eq(0)
      end

  end

  describe ".parse_sale_price" do
    it "it should get sale price" do
      price = Price.parse_sale_price('<span class="a-size-large a-color-price olpOfferPrice a-text-bold">  $12.99   </span>')
      expect(price).to eq(12.99)
    end
  end

  describe ".parse_shipping_fee" do
    context 'pass raw html string' do
      it "free shipping get shipping fee 0" do
        shipping_fee = Price.parse_shipping_fee('<b>FREE Shipping</b>')
        expect(shipping_fee).to eq(0)
      end

      it "should works on non free shipping" do
        html = '<span class="olpShippingPrice"> $5.54 </span>'
        shipping_fee = Price.parse_shipping_fee(html)
        expect(shipping_fee).to eq(5.54)
      end
    end


    context 'pass nokogiri doc' do

       it "free shipping get shipping fee 0" do
        doc = Nokogiri::HTML(price_html_block)

        shipping_fee = Price.parse_shipping_fee(doc.at_css('.olpShippingInfo'))
        expect(shipping_fee).to eq(0)
      end

      it "should works on non free shipping" do
        doc = Nokogiri::HTML(price_html_block1)
        shipping_fee = Price.parse_shipping_fee(doc.at_css('.olpShippingInfo'))
        expect(shipping_fee).to eq(5.54)
      end
    end

  end

end