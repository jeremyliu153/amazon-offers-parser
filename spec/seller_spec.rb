require_relative '../lib/seller.rb'

RSpec.describe Seller do
  seller_normal_html_block = '<div class="a-column a-span2 olpSellerColumn" role="gridcell"> 
  <h3 class="a-spacing-none olpSellerName"> 
    <span class="a-size-medium a-text-bold"> <a href="/gp/aag/main/ref=olp_merch_name_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05">D M Books Store</a> </span>
  </h3> 
  <p class="a-spacing-small"> <i class="a-icon a-icon-star a-star-4-5"><span class="a-icon-alt">4.5 out of 5 stars</span></i> <a href="/gp/aag/main/ref=olp_merch_rating_2?ie=UTF8&amp;asin=0761455299&amp;isAmazonFulfilled=0&amp;seller=A19A29L7PVBL05"><b>92% positive</b></a> over the past 12 months. (409 total ratings) <br> </p> 
</div>'
  seller_amazon_html_block = '<div class="a-column a-span2 olpSellerColumn" role="gridcell">
  <h3 class="a-spacing-none olpSellerName"><img alt="Amazon.com" src="https://images-na.ssl-images-amazon.com/images/I/01dXM-J1oeL.gif">
  </h3>
</div>'
  seller_just_launched_html_block = '<div class="a-column a-span2 olpSellerColumn" role="gridcell">
  <h3 class="a-spacing-none olpSellerName"><span class="a-size-medium a-text-bold"><a href="/gp/aag/main/ref=olp_merch_name_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Kathysshop</a></span>
  </h3>
  <p class="a-spacing-small"><b class="olpJustLaunched">Just Launched</b>(<span class="olpSellerProfile"><a href="/gp/aag/main/ref=olp_merch_new_seller_7?ie=UTF8&amp;asin=0545261244&amp;isAmazonFulfilled=0&amp;seller=AYXDDSK9W0RO9">Seller Profile</a></span>)</p>
</div>'
  

  
  describe '#initialize' do
    # normal:
    seller = Seller.new(seller_normal_html_block)
    it "should parse normal seller name" do
       expect(seller.seller_name).to eq("D M Books Store")
       puts "seller_normal_name is %s" %seller.seller_name
    end
      
    it "should parse normal seller id" do
       expect(seller.seller_id).to eq("A19A29L7PVBL05")
       puts "seller_normal_id is %s" %seller.seller_id
    end

    it "should parse normal seller feedback" do
       expect(seller.feedback == 92.0)
       puts "seller_normal_feedback is %.1d" %seller.feedback
    end
    
    it "should parse normal seller ratings" do
       expect(seller.ratings == 409.0)
       puts "seller_normal_ratings is %s" %seller.ratings
    end 
  
    # amazon:
    amazon_seller = Seller.new(seller_amazon_html_block)
    it "should parse amazon seller name" do
       expect(amazon_seller.seller_name).to eq("Amazon")
       puts "seller_amazon_name is %s" %amazon_seller.seller_name
    end
      
    it "should parse amazon seller id" do
       expect(amazon_seller.seller_id).to eq(nil)
       puts "seller_amazon_id is nil"
    end

    it "should parse amazon seller feedback" do
       expect(amazon_seller.feedback == 0)
       puts "seller_amazon_feedback is nil"
    end
    
    it "should parse amazon seller ratings" do
       expect(amazon_seller.ratings == 0)
       puts "seller_amazon_ratings is nil"
    end 

    # launched:
    launched_seller = Seller.new(seller_just_launched_html_block)
    it "should parse launched seller name" do
       expect(launched_seller.seller_name).to eq("Kathysshop")
       puts "seller_launched_name is %s" %launched_seller.seller_name
    end
      
    it "should parse launched seller id" do
       expect(launched_seller.seller_id).to eq("AYXDDSK9W0RO9")
       puts "seller_launched_id is %s" %launched_seller.seller_id
    end

    it "should parse launched seller feedback" do
       expect(launched_seller.feedback == 0)
       puts "seller_launched_feedback is nil"
    end
    
    it "should parse launched seller ratings" do
       expect(launched_seller.ratings == 0)
       puts "seller_launched_ratings is nil"
    end


  end



end