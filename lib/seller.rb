require 'nokogiri'

class Seller

def initialize(seller_html)
  	@seller_html = seller_html
  end

  def seller_name
  	parse() if @seller_name.nil?
  	@seller_name
  end

  def seller_id
  	parse() if @seller_id.nil?
  	@seller_id
  end

  def feedback
  	parse() if @feedback.nil?
  	@feedback
  end

  def ratings
  	parse() if @ratings.nil?
  	@ratings
  end

  def parse()
    doc = (@seller_html.is_a? (Nokogiri::XML::Element)) ? @seller_html : (Nokogiri::HTML(@seller_html))
  	seller_name = doc.at_css('a')
  	@seller_name = Seller.parse_seller_name(seller_name)
  	seller_id = doc.at_css('a')
  	@seller_id = Seller.parse_seller_id(seller_id)
  	feedback = doc.at_css('b')
  	@feedback = Seller.parse_feedback(feedback)
  	ratings = doc.at_css('p')
  	@ratings = Seller.parse_ratings(ratings)
  end

  def self.parse_seller_name(seller_name)
    
    
    if (seller_name.class == NilClass)
          seller_name = "Amazon"
    # elsif (seller_name.is_a? (Nokogiri::XML::Element))
          # seller_name = "Amazon-Warehouse"
        # @seller_name = seller_name.css("img").first['alt']
        # puts "************"
        # puts @seller_name
        # puts "**************"
    else
          seller_name.text
    end  	
  end

  def self.parse_seller_id(seller_id)
    if (seller_id.class == NilClass)
      seller_id = nil
    # elsif (seller_id.is_a? (Nokogiri::XML::Element))
    #       seller_id = nil
    else
      href_str = seller_id.xpath("@href").to_s
      seller_id = href_str.split("&")[-1].split("seller=")[-1].to_s
    end
  	
  end

  def self.parse_feedback(feedback)
    if (feedback.class == NilClass)
      feedback = nil
    # elsif (feedback.is_a? (Nokogiri::XML::Element))
    #       feedback = nil
    else
      doc = feedback.is_a?(String)? Nokogiri::HTML(feedback): feedback
      feedback = doc.text.gsub(/[^0-9\.]/, '').to_f
    end
  	
  end

  def self.parse_ratings(ratings)
    if (ratings.class == NilClass)
      ratings = nil
    elsif (ratings.is_a? (Nokogiri::XML::Element))
          ratings = nil
    else
  	ratings = ratings.text[/\(.*?\)/].gsub(/[^0-9\.]/, '').to_f
    end
  	
  end

end 
