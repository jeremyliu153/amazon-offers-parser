require 'nokogiri'

class Condition
   attr_reader :condition, :sub_condition

   def initialize(html)
	 @html = html.is_a?(String) ? Nokogiri::HTML(html): html
   end

   def parse()
   	parse_new if new?
   	parse_used if used?
   end
   
   def new?
   	 !!@html.at_css('#olpNew')
   end

   def used?
   	 !!@html.at_css('#olpUsed')
   end

   private

     def parse_new
     	@condition = "NEW"
     	@sub_condition = "NEW"
     end

     def parse_used
     	@condition = @html.at_css('.olpCondition').children.first.text.gsub(/\W/, '')
     	@sub_condition = @html.at_css('#offerSubCondition').text.gsub(/\W/, '')
     end

end