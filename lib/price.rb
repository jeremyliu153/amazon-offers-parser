require 'nokogiri'

class Price
  
  def initialize(price_html)
  	@price_html = price_html
  end
  
  def sale_price
  	parse() if @sale_price.nil?
  	@sale_price
  end

  def parse_shipping_fee
  	parse() if @parse_shipping_fee.nil?
  	@parse_shipping_fee
  end


  def parse()
    doc = (@price_html.is_a? (Nokogiri::XML::Element)) ? @price_html : (Nokogiri::HTML(@price_html))
  	sale_price = doc.at_css('.olpOfferPrice')
  	shipping_fee = doc.at_css('.olpShippingInfo')
  	@sale_price = Price.parse_sale_price(sale_price)
  	@parse_shipping_fee = Price.parse_shipping_fee(shipping_fee)
  end

  def self.parse_sale_price(sale_price)
  	if (sale_price.is_a?(String))
  		doc = Nokogiri::HTML(sale_price)
  	else
  		doc = sale_price
  	end
  	doc.text.gsub(/[^0-9\.]/, '').to_f
  end

  def self.parse_shipping_fee(shipping_fee)
  	doc = shipping_fee.is_a?(String) ? Nokogiri::HTML(shipping_fee) : shipping_fee
  	free_shipping = doc.at_css('b')
  	if (free_shipping)
  		0
  	else
  		shipping_fee_doc = doc.at_css('.olpShippingPrice')
  		shipping_fee_doc.text.gsub(/[^0-9\.]/, '').to_f
  	end
  end

end