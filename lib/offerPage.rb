require 'nokogiri'
require_relative '../lib/offer.rb'

class OfferPage
	attr_reader :offer_arr
    def initialize(offer_page_directory)
    	@offer_page_directory = offer_page_directory
    end

    def offer_arr
	  	parse_offer() if @offer_arr.nil?
	  	@offer_arr
    end

    def parse_offer()
    	doc = File.open(@offer_page_directory) { |f| Nokogiri::HTML(f) }
    	@offer_arr = doc.css("//div").select{ |link| link['class'] == "a-row a-spacing-mini olpOffer" }
    	offer_arr.each_with_index do |offer_html, i| 
    	puts "offer#{i+1}: "
    	offer = Offer.new(offer_html).parse
    	puts "-----------------------------------------------"
        end
    end
end