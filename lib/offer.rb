require 'nokogiri'
require_relative '../lib/price.rb'
require_relative '../lib/condition.rb'
require_relative '../lib/seller.rb'


class Offer

    def initialize(offer_html)
    	@offer_html = offer_html
    end

	def price
	  	parse() if @price.nil?
	  	@price
    end

	def condition
	  	parse() if @condition.nil?
	  	@condition
	end

	def seller
	  	parse() if @seller.nil?
	  	@seller
	end

	def parse ()
	    doc = (@offer_html.is_a? (Nokogiri::XML::Element)) ? @offer_html : (Nokogiri::HTML(@offer_html))
	  	price = doc.at_css('.olpPriceColumn')
	  	@price = Offer.parse_price(price)
	  	condition = doc.at_css('.olpConditionColumn')
	  	@condition = Offer.parse_condition(condition)
	  	seller = doc.at_css('.olpSellerColumn')
	  	@seller = Offer.parse_seller(seller)

	end

    def self.parse_price (price)
        price = Price.new(price)
        sale_price = price.sale_price
        shipping_fee = price.parse_shipping_fee
        puts "sale_price:#{sale_price}------shipping_fee:#{shipping_fee}"

    end

    def self.parse_condition (condition)
    	condition = Condition.new(condition)
    	condition.parse
        puts "condition:#{condition.condition}-------sub_condition:#{condition.sub_condition}"
    end

    def self.parse_seller (seller)
    	seller = Seller.new(seller)
    	seller.parse
        puts "seller_name: #{seller.seller_name}--------seller_id: #{seller.seller_id},\n feedback:#{seller.feedback}------ratings:#{seller.ratings}"
    end


end